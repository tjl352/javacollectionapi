package com.collectioninterface.list;

import java.util.ArrayList;
import java.util.Iterator;

public class YourArrayLists {
  public static void main(String[] args) {
	  ArrayList<Names> al = new ArrayList<Names>();
	  al.add(new Names("thomas"));
	  al.add(new Names("bobby"));
	  al.add(new Names("lisa"));
	  
	  System.out.println(al);
	  
	  for(Names s : al){
		  System.out.println(al.toString());
	  }
	  
	  Iterator it = al.iterator();
	  while(it.hasNext()){
		  System.out.println(it.next());
	  }
  }
}

class Names{
	String name = "";
	
	public Names(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString(){
		return name;
	}
}