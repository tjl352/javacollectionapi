package com.collectioninterface.list;

import java.util.ArrayList;
import java.io.Console;
import java.util.Iterator;
import java.util.Scanner;

public class YourArrayListsApps {

	public static void main(String[] args) {

		ArrayList<Names> al = new ArrayList<Names>();
		Scanner in = new Scanner(System.in);

		while (true) {
			System.out.println("Enter a name: ");
			al.add(new Names(in.next()));
			for (Names n : al) {
				System.out.println("Print ArrayList" + "" + n.toString());
			}
		}

	}
}
// reusing Names component bean from com.collectioninterface.list.YourArrayLists
