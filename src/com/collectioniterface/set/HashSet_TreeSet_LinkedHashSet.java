package com.collectioniterface.set;

import java.util.*;

public class HashSet_TreeSet_LinkedHashSet {
	// Sets not synchronized
	// duplicates are not inserted
	public static void main(String args[]) {
		// create a hash set
		HashSet<String> hs = new HashSet<String>();
		// add elements to the hash set
		// add() method
		hs.add("B");
		hs.add("A");
		hs.add("D");
		hs.add("E");
		hs.add("C");
		hs.add("A");
		System.out.println("HashSet hs: " + hs);

		// remove an element from the data structure
		// use the boolean remove();
		hs.remove("C");
		System.out.println("Element 'C' has been removed from HashSet hs");

		// size method, duplicates was not added
		System.out.println("HastSet hs size: " + hs.size());

		// to clear all elements
		// void clear()

		// to clone the HashSet without its elements
		// Object clone() ; returns a shallow copy
		System.out.println("This is a clone of hs: " + hs.clone());

		// see if an object is in the data structure
		// use the boolean contains() method
		System.out.println("Does HashSet hs contain 'F'? " + hs.contains("F"));
		System.out.println("Does HasShet hs contain 'A'? " + hs.contains("A"));

		// see if the Hashet has no elements
		// boolean isEmpty()
		System.out.println("Is HashSet hs empty of elemnts? " + hs.isEmpty());

		// iterating through the HashSet without the Iterator object
		// same performance as Iterator, but much better to read
		for (String str : hs) {
			System.out
					.println("Looping over HashSet without the Iterator, but with advanced for loop: "
							+ str);
		}

		// to iterate through the HashSet with the Iterator object
		// use the Iterator iterator() it returns a iterator object over the
		// elements in the HashSet
		Iterator<String> itr = hs.iterator();
		// boolean hasNext() returns true/false if there are more elements
		while (itr.hasNext()) {
			// Object next() returns the next element
			System.out.println("Iterating over HashSet using Iterator: "
					+ itr.next());
		}
		// void remove() removes the current element

		// sort hashSet using a List
		// sorted alphabetically
		List sortedList = new ArrayList(hs);
		Collections.sort(sortedList);
		System.out.println(sortedList);

		// create a hash set
		// automatically ordered int for ascending order
		Set hs1 = new HashSet();
		// add elements to the hash set
		hs1.add(5);
		hs1.add(4);
		hs1.add(1);
		hs1.add(7);
		hs1.add(3);
		hs1.add(9);
		System.out.println(hs1);

		// Use TreeSet to order a set (natural ascending)
		// slowest due to ordering, HashSet and LinkedSet faster and about the
		// same speed
		Set ts = new TreeSet();
		ts.add(5000);
		ts.add(2000);
		ts.add(3000);
		System.out.println(ts);

		// Use LinkedHashSet to manipulate the data structure
		// LinkedHashSet is ordered. It maintains the order in which they were
		// inserted.
		LinkedHashSet lhs = new LinkedHashSet();
		lhs.add("A");
		lhs.add("D");
		lhs.add("F");
		System.out.println(lhs);

	}
}