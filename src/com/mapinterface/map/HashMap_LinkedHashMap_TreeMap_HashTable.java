package com.mapinterface.map;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class HashMap_LinkedHashMap_TreeMap_HashTable {
	// The Map Interface provides key-value pairs for implementations
	// Each Map implementation has its specification

	public static void main(String[] args) {

		// Implements with HashMap with its methods
		// Create a hash map
		HashMap hm = new HashMap();
		// Put elements to the map
		hm.put("Zara", new Double(3434.34));
		hm.put("Mahnaz", new Double(123.22));
		hm.put("Ayan", new Double(1378.00));
		hm.put("Daisy", new Double(99.22));
		hm.put("Qadir", new Double(-19.08));

		// Get a set of the entries
		Set set = hm.entrySet();
		// Get an iterator
		Iterator i = set.iterator();
		// Display elements
		System.out
				.println("---------------------HashMap Implementation-------------------");

		while (i.hasNext()) {
			Map.Entry me = (Map.Entry) i.next();
			System.out.print(me.getKey() + ": ");
			System.out.println(me.getValue());
		}
		System.out.println();
		// Deposit 1000 into Zara's account
		double balance = ((Double) hm.get("Zara")).doubleValue();
		hm.put("Zara", new Double(balance + 1000));
		System.out.println("Zara's new balance: " + hm.get("Zara"));

		// separate implementations
		System.out
				.println("--------------------------------------------------------------");

		// Implements with LinkedHashMap with its methods
		// Create a hash map
		LinkedHashMap lhm = new LinkedHashMap();
		// Put elements to the map
		lhm.put("Zara", new Double(3434.34));
		lhm.put("Mahnaz", new Double(123.22));
		lhm.put("Ayan", new Double(1378.00));
		lhm.put("Daisy", new Double(99.22));
		lhm.put("Qadir", new Double(-19.08));

		// Get a set of the entries
		Set set3 = lhm.entrySet();
		// Get an iterator
		Iterator i3 = set.iterator();
		// Display elements
		System.out.println();
		System.out
				.println("--------------------LinkedHashMap Implementation--------------------");
		while (i3.hasNext()) {
			Map.Entry me3 = (Map.Entry) i3.next();
			System.out.print(me3.getKey() + ": ");
			System.out.println(me3.getValue());
		}
		System.out.println();
		// Deposit 1000 into Zara's account
		double balance3 = ((Double) lhm.get("Zara")).doubleValue();
		lhm.put("Zara", new Double(balance + 1000));
		System.out.println("Zara's new balance: " + lhm.get("Zara"));
		// separate implementations
		System.out
				.println("--------------------------------------------------------------");

		// Implements with TreeMap with its methods
		// Create a hash map
		TreeMap tm = new TreeMap();
		// Put elements to the map
		tm.put("Zara", new Double(3434.34));
		tm.put("Mahnaz", new Double(123.22));
		tm.put("Ayan", new Double(1378.00));
		tm.put("Daisy", new Double(99.22));
		tm.put("Qadir", new Double(-19.08));

		// Get a set of the entries
		Set set1 = tm.entrySet();
		// Get an iterator
		Iterator i1 = set1.iterator();
		// Display elements
		System.out.println();
		System.out
				.println("---------------------TreeMap Implementation-------------------");
		while (i1.hasNext()) {
			Map.Entry me1 = (Map.Entry) i1.next();
			System.out.print(me1.getKey() + ": ");
			System.out.println(me1.getValue());
		}
		System.out.println();
		// Deposit 1000 into Zara's account
		double balance1 = ((Double) tm.get("Zara")).doubleValue();
		tm.put("Zara", new Double(balance1 + 1000));
		System.out.println("Zara's new balance: " + tm.get("Zara"));

		// separate implementations
		System.out
				.println("--------------------------------------------------------------");

		// Implements with HashTable with its methods
		// Create a hash map
		Hashtable balance4 = new Hashtable();
		Enumeration names;
		String str;
		double bal;

		balance4.put("Zara", new Double(3434.34));
		balance4.put("Mahnaz", new Double(123.22));
		balance4.put("Ayan", new Double(1378.00));
		balance4.put("Daisy", new Double(99.22));
		balance4.put("Qadir", new Double(-19.08));

		// Show all balances in hash table.
		System.out.println();
		System.out
				.println("---------------------HashTable Implementation-------------------");
		names = balance4.keys();
		while (names.hasMoreElements()) {
			str = (String) names.nextElement();
			System.out.println(str + ": " + balance4.get(str));
		}
		System.out.println();
		// Deposit 1,000 into Zara's account
		bal = ((Double) balance4.get("Zara")).doubleValue();
		balance4.put("Zara", new Double(bal + 1000));
		System.out.println("Zara's new balance: " + balance4.get("Zara"));
	}
}
